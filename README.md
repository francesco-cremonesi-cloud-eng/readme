The purpose of this exercise is to deploy Gitlab on an AWS EKS cluster and showcase the Review Apps feature.

# Architecture

The deployed Gitlab installation runs on an AWS EKS cluster, with an nginx ingress controller acting as a load balancer. The Gitlab instance controls one (or more) runners that execute CI/CD pipelines, each runner deployed to a Kubernetes pod. A private hosted zone manages DNS name resolution within the Virtual Private Cloud to enable access to the gitlab.example.com domain.

Any developer with access to an instance within the same VPC is able to commit repositories to the Gitlab installation at gitlab.example.com and execute any CI/CD pipelines on the Kubernetes cluster. See Figure below for a graphical representation.


![architecture](images/arch.png)

### Design choices

#### EKS cluster provisioning

This ![example](https://gitlab.com/gitops-demo/infra) from the official Gitlab ![issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/220201) provides the necessary code to provision the EKS cluster and Gitlab provider using Terraform.
*Note* that I had to increase the instance type because the total number of pods is ![limited](https://dev.to/wingkwong/how-to-fix-insufficient-pods-issue-when-deploying-to-amazon-eks-d35) by the number of Elastic Network Interfaces of an instance.

#### Installing Gitlab via Helm chart

I decided to install Gitlab using the Cloud Native Helm Chart because it is the official way as ![recommened](https://docs.gitlab.com/charts/) in the docs.

#### Disabling ssl certificates

Gitlab requires an available domain name for the installation. Since I do not own one, I decided to disable ssl security where possible and provide self-signed certificates where needed. This is obviously *very dangerous* and only acceptable in a development environment such as this one.

#### Using a private hosted zone

Since the installation still requires a valid domain, I provisioned a private hosted zone linked to the same VPC as the EKS cluster, and created records for the address `gitlab.example.com` pointing to the ingress load balancer.

# Deployment

The EKS cluster is provisioned used the official aws `eks` terraform module. Then Gitlab is installed with the cloud native Helm chart. Most of the AWS infrastructure is provisioned by Terraform as *Infrastructure as code*. One notable exception is the creation of the route 53 record for `gitlab.example.com`, which needs to be performed manually for reasons explained below.
The installation of the Helm chart is also executed by Terraform, but the installation of the Gitlab runner has to be done manually using the `kubectl` command.

## Preparation

An AWS profile and a Gitlab token are required.
The AWS profile can be obtained with the `aws configure` command after installing the aws CLI.
The Gitlab token can be created following these ![instructions](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

Once preparation is complete, all the non-manual steps presented below are taken care of by running `terraform apply` in the command line on your local machine. Below is a step-by-step explanation of the mnost relevant implementation details.

## EKS cluster provisioning

The `vpc.tf, eks.tf` files handle all the heavy lifting for provisioning the EKS cluster. They rely on the official aws modules for Terraform and require only minimal configuration.


## Gitlab deployment

### The Gitlab Terraform provider

This provider is used to create a service account and to link it to the EKS cluster.
The file `gitlab-admin.tf` serves this purpose.
Additionally, following the implementation of ![Brad Downey](https://gitlab.com/gitops-demo/infra), Gitlab Managed Apps are managed through the `cluster-management` repository, as handled in the `group_cluster.tf` file. 

### Gitlab installation via Helm chart

Using Terraform's Helm provider, I install the Gitlab chart in the `helm.tf` file. As explained above, heavy configuration is required to workaround the need of providing a valid domain name and SSL certificates.

### Manual step: creating the DNS record for gitlab.example.com

While the private hosted zone has been provided with Terraform, I was unable to create the necessary DNS record automatically.
The reason for this is that I was unable to access the Nginx Ingress controller's hostname or IP through Terraform.
Instead, the hostname can be obtained from the command line by running (on your local machine)
```
kubectl describe service gitlab-nginx-ingress-controller | grep "LoadBalancer Ingress" | awk -F":" '{print $2}'
```
Then one can create the record with the AWS web UI or uncomment and update the relevant section in the `route53.tf` file and re-run `terraform apply`.

### First milestone: visit gitlab.example.com

After the correct Route53 record has been created and propagated in the VPC, you should be able to visit `http://gitlab.example.com`. Note that you can only do so from an instance within the VPC, so the simplest way is to spawn a new EC2 instance and install the required packages for X11 forwarding plus a web browser.

![login](images/gitlab-on-remote-premise-login.png)

### Manual step: installing Gitlab Runner

Installing Gitlab runner requires one to visit `http://gitlab.example.com/admin/runners` and copy the registration token, as shown in the image below.

![registration-token](images/gitlab-on-remote-premise-runner-token.png)

With this token, I create a kubernetes secret
```
kubectl create secret generic gitlab-runner-gitlab-runner-secret --from-literal=runner-registration-token=<your token> --from-literal=runner-token=""
```

Additionally, the cluster ca certificate is needed in spite of the fact that I tried to disable all certificate and https security. The (self-signed) certificate is found, saved to file and bundled in another secret with the required format with
```
kubectl get secret gitlab-wildcard-tls-ca -ojsonpath='{.data.cfssl_ca}' | base64 --decode > gitlab.example.com.ca.pem
kubectl create secret generic gitlab-runner-certificates --from-file=gitlab.example.com.crt=./gitlab.example.com.ca.pem
```
We are finally ready to install gitlab runner from the ![repository](https://charts.gitlab.io/)

```
helm install gitlab-runner gitlab/gitlab-runner --set gitlabUrl="http://gitlab.example.com" --set runners.secret="gitlab-runner-gitlab-runner-secret" --set certsSecretName="gitlab-runner-certificates" --set rbac.create="true"
```

# Review apps

After a Gitlab runner has been succesfully provisioned, it becomes possible to enable the Review Apps feature for any project.
To enable the feature a configuration needs to be added to the `.gitlab-ci.yml` file, as shown in this ![changeset](https://gitlab.com/francesco-cremonesi-cloud-eng/apps/docker-hello-world/-/commit/f15c8dcb9af2bd4606d27403be71d264b3bfad37).

## Showcase review apps

To see the feature in action, one then needs to submit any minor change to any branch except `master`. Review Apps will automatically create a separate	environment where the app is deployed, sort of like a staging environment where one can test out new changes before merging them. The images below demonstrate the successful creation of an environment named `review/test-review-apps` and a merge request where the `View latest app` button is visible.

![environment](images/gitlab-on-remote-premise-review-success-environment.png)
![view-app-button](images/gitlab-on-remote-premise-review-success-merge-request.png)


# Challenges and next steps

The first thing that requires fixing is record creation as *Infrastructure as code* through Terraform.
I believe it should be possible to issue the `kubectl .. | grep .. | awk ..` command utilized before through Terraform, in order to collect the load balancer's hostname. However, I haven't had time to look into that yet.

A second issue is that if I click on the `View latest app` button right now I get a browser error, most likely caused by a 404 error. My suspicion is that the review environment is destroyed immediately since the script that I specified in the `.gitlab-ci.yml` file is a simple `echo` command. If that's not the culprit of the problem, than I would start looking into whether the address is correctly resolved by using a tool such as `dig`.

Finally, the issue of security and specifically ssl certificates is very important and should not be taken lightly. However, since the Gitlab Helm chart comes with a Let's Encrypt installation, once a valid public domain name becomes available to be used then it would be simply possible to provide that domain name to Let's Encrypt, and let it generate all the necessary SSL certificates.



